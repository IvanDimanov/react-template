import Joi from 'joi'

const envVarSchema = {
  REACT_APP_API_SERVER: Joi
    .string()
    .uri()
    .required(),

  REACT_APP_QUERY_STATE_TIME: Joi
    .number()
    .integer()
    .min(0)
    .max(30 * 24 * 60 * 60 * 1000),
}


const { error } = Joi.object()
  .keys(envVarSchema)
  .required()
  .validate(process.env, {
    abortEarly: false,
    allowUnknown: true, // We need to allow unknown ENV VARs
                        // since Webpack is using some custom ENV VARs such as `FAST_REFRESH`
  })


if (error) {
  throw new ReferenceError(`Invalid ENV VAR:
${error.details
    .map(({ message, context }) => `  ${message}; currently ${context?.key}=${context?.value}`)
    .join('\n')}
\n`)
}
