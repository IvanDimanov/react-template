import axiosClient from '@src/api/axiosClient'
import validateResponse from '@src/api/validateResponse'

import {
  GetPersonResponse,
} from './types'


const peopleApi = {
  getPerson: async (
    id: number,
  ): Promise<GetPersonResponse> => axiosClient
    .get(`people/${id}`)
    .then(validateResponse(GetPersonResponse)),
}


export default peopleApi
