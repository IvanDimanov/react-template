# React template
This project is meant to be used a simple template to create scalable React Apps.


## [Live Demo](https://react-template-ivandimanov.vercel.app)
## [![App](https://gitlab.com/IvanDimanov/react-template/-/raw/main/image.png)](https://react-template-ivandimanov.vercel.app)


## Running locally
```
git clone git@gitlab.com:IvanDimanov/react-template.git
cd react-template
yarn
yarn start
```


## Tech stack
- React with [CRA](https://create-react-app.dev) + [TypeScript](https://www.typescriptlang.org)
- [React-Query](https://react-query.tanstack.com) + [Zustand](https://github.com/pmndrs/zustand)
- [TailwindCSS](https://tailwindcss.com)
