
import { nonEmptyString, uri } from '@src/api/validationDecorators'

/**
 * Single Species data response from SWAPI
 */
export class GetSpeciesResponse {
  @nonEmptyString
  name: string

  @nonEmptyString
  classification: string

  @nonEmptyString
  designation: string

  @uri
  url: string
}
