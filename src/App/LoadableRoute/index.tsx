import { Suspense } from 'react'
import PropTypes from 'prop-types'
import { Route } from 'react-router-dom'

import ErrorBoundary from './ErrorBoundary'
import DelayedFallback from './DelayedFallback'

const EmptyLayout = ({ children }) => <>{children}</>

EmptyLayout.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
}

EmptyLayout.defaultProps = {
  children: undefined,
}


/**
 * This wrapper helps with lazy loading of page,
 * encapsulates errors, adds optional Page Layout,
 * and show a Loading component within a respectful delay.
 *
 * @param {object} props
 * @return {object} React component
 */
const LoadableRoute = ({
  component: Component,
  routeComponent: RouteComponent = Route,
  layout: Layout,
  ...options
}) => {
  const PageComponent = () => (
    <ErrorBoundary>
      <Layout {...options}>
        <ErrorBoundary>
          <Suspense fallback={<DelayedFallback />}>
            <Component />
          </Suspense>
        </ErrorBoundary>
      </Layout>
    </ErrorBoundary>
  )

  return <RouteComponent {...options} component={PageComponent} />
}


LoadableRoute.propTypes = {
  path: PropTypes.string,
  component: PropTypes.object.isRequired,
  routeComponent: PropTypes.oneOfType([PropTypes.node, PropTypes.func]),
  exact: PropTypes.bool,
  layout: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
    PropTypes.func,
  ]),
}

LoadableRoute.defaultProps = {
  path: '',
  routeComponent: Route,
  exact: false,
  layout: EmptyLayout,
}

LoadableRoute.displayName = 'LoadableRoute'


export default LoadableRoute
