import * as jf from 'joiful'

const integer = jf.number().integer().min(0).required()

export default integer
