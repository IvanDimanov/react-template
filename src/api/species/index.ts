import axiosClient from '@src/api/axiosClient'
import validateResponse from '@src/api/validateResponse'

import {
  GetSpeciesResponse,
} from './types'


const speciesApi = {
  getSingleSpecies: async (
    id: number,
  ): Promise<GetSpeciesResponse> => axiosClient
    .get(`species/${id}`)
    .then(validateResponse(GetSpeciesResponse)),
}


export default speciesApi
