import PropTypes from 'prop-types'

import { VscLoading } from 'react-icons/vsc'
import { IoAccessibility } from 'react-icons/io5'
import { GiBodyHeight } from 'react-icons/gi'
import { HiEye } from 'react-icons/hi'
import { FaWeightHanging } from 'react-icons/fa'

import { GetPersonResponse } from '@src/api/people/types'
import { GetSpeciesResponse } from '@src/api/species/types'


type PersonProps = {
  className?: string
  person?: GetPersonResponse
  species?: GetSpeciesResponse
  isLoading: boolean
}

const PersonTable = ({
  className,
  person,
  species,
  isLoading,
}: PersonProps) => {
  if (!person ) {
    return (
      <div className={className}>
        <div className="flex justify-center items-center border border-gray-300 rounded mx-auto p-6 w-128 h-48">
          {isLoading ? (
            <div className="flex justify-center items-center">
              Loading person`s data <VscLoading className="text-2xl animate-spin ml-4" />
            </div>
          ) : (
            <div>
              No person loaded
            </div>
          )}
        </div>
      </div>
    )
  }

  return (
    <div className={className}>
      <div className="border border-gray-300 rounded mx-auto p-6 w-128 h-48">
        <div className="flex items-center">
          <div className="font-bold text-lg">
            {person?.name}
          </div>

          {isLoading ? (
            <VscLoading className="text-2xl animate-spin ml-4" />
          ) : null}
        </div>

        <div className="grid grid-cols-2 gap-6 mt-6 capitalize">
          <div className="flex items-center space-x-2">
            <IoAccessibility />
            <span>
              {species?.name || 'N/A'}
            </span>
          </div>

          <div className="flex items-center space-x-2">
            <GiBodyHeight />
            <span>
              {person?.height}
              <em>cm</em>
            </span>
          </div>

          <div className="flex items-center space-x-2">
            <HiEye />
            <span>
              {person?.eye_color}
            </span>
          </div>

          <div className="flex items-center space-x-2">
            <FaWeightHanging />
            <span>
              {person?.mass}
              <em>kg</em>
            </span>
          </div>
        </div>
      </div>
    </div>
  )
}


PersonTable.propTypes = {
  className: PropTypes.string,
  person: PropTypes.object,
  species: PropTypes.object,
  isLoading: PropTypes.bool,
}

PersonTable.defaultProps = {
  className: undefined,
  person: undefined,
  species: undefined,
  isLoading: false,
}

PersonTable.displayName = 'PersonTable'


export default PersonTable
