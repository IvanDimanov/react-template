import * as jf from 'joiful'

import { integer, nonEmptyString, uri } from '@src/api/validationDecorators'

/**
 * Single Person data response from SWAPI
 */
export class GetPersonResponse {
  @nonEmptyString
  name: string

  @integer
  height: number

  @nonEmptyString
  mass: string

  @nonEmptyString
  hair_color: string

  @nonEmptyString
  skin_color: string

  @nonEmptyString
  eye_color: string

  @nonEmptyString
  gender: string

  @uri
  homeworld: string

  @jf.array().items((joi) => joi.string().uri().required())
  films: string[]

  @jf.array().items((joi) => joi.string().uri())
  species: string[]

  @jf.array().items((joi) => joi.string().uri())
  vehicles: string[]

  @jf.array().items((joi) => joi.string().uri())
  starships: string[]

  @uri
  url: string
}
