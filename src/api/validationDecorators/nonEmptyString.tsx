import * as jf from 'joiful'

const nonEmptyString = jf.string().min(1).max(512).required()

export default nonEmptyString
