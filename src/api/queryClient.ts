import { QueryClient } from 'react-query'
import { AxiosError } from 'axios'

import ApiResponseValidationError from '@src/api/ApiResponseValidationError'

/**
 * Sometimes the BackEnd throws an error when the make an API call.
 * By default, `react-query` will always retry the same query a couple of times
 * before throwing the BackEnd error.
 * `PERMANENT_ERROR_CODES` is a list of all BackEnd Error response codes
 * which we know that will make the BackEnd throw the same error again
 * if we just retry the same query.
 */
const PERMANENT_ERROR_CODES = [400, 401, 403, 404]
const MAX_ERROR_RETRIES = 3

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      staleTime: parseInt(process.env.REACT_APP_QUERY_STATE_TIME as string, 10),

      /**
       * This function will tell if `react-query` should retry a failed BackEnd API call.
       *
       * @param {number} failureCount Which error attempt we'll retry
       * @param {AxiosError} error What was the latest BackEnd error
       * @return {boolean} Should we retry the same query again
       */
      retry: (failureCount, error) => {
        if (error instanceof ApiResponseValidationError) {
          return false
        }

        const axiosError = error as AxiosError
        if (PERMANENT_ERROR_CODES.includes(axiosError.response?.status || 0)) {
          return false
        }

        return failureCount <= MAX_ERROR_RETRIES
      },
    },
  },
})

export default queryClient
