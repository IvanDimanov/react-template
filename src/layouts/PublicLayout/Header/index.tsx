import usePerson, { selectPerson } from '@src/globalState/usePerson'

import LinkButton from './LinkButton'


const Header = () => {
  const person = usePerson(selectPerson)

  return (
    <nav className="flex justify-between items-center bg-gray-200">
      <div className="w-96" />

      <div className="text-gray-500 text-lg py-5 space-x-16">
        <LinkButton
          path="/home"
          label="Home"
        />

        <LinkButton
          path="/people"
          label="People"
        />

        <LinkButton
          path="/test-404"
          label="Test 404"
        />
      </div>

      <div className="whitespace-nowrap w-96 px-4">
        <span>Last loaded person: </span>
        <b>{person?.name || 'N/A'}</b>
      </div>
    </nav>
  )
}


export default Header
