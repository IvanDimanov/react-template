import { ReactNode } from 'react'
import PropTypes from 'prop-types'

import Header from './Header'

type PublicLayoutProps = {
  children: ReactNode
}

const PublicLayout = ({
  children,
}: PublicLayoutProps) => (
  <div className="h-full">
    <Header />

    <main className="mx-auto pt-16 w-192">
      {children}
    </main>
  </div>
)


PublicLayout.propTypes = {
  children: PropTypes.node.isRequired,
}

PublicLayout.displayName = 'PublicLayout'


export default PublicLayout
