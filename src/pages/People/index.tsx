import { useEffect, useMemo } from 'react'
import { useQuery, QueryFunction, QueryKey } from 'react-query'
import { useParams } from 'react-router-dom'
import { AxiosError } from 'axios'

import { toast } from 'react-toastify'

import usePerson, { selectSetPerson } from '@src/globalState/usePerson'

import peopleApi from '@src/api/people'
import { GetPersonResponse } from '@src/api/people/types'

import speciesApi from '@src/api/species'
import { GetSpeciesResponse } from '@src/api/species/types'

import PersonTable from './PersonTable'
import Buttons from './Buttons'


const getPerson: QueryFunction<GetPersonResponse, QueryKey> =
  ({ queryKey: [, id] }) => peopleApi.getPerson(id as number)

const getSingleSpecies: QueryFunction<GetSpeciesResponse, QueryKey> =
  ({ queryKey: [, id] }) => speciesApi.getSingleSpecies(id as number)


const People = () => {
  const { urlPersonId } = useParams<{ urlPersonId: string }>()
  const setPerson = usePerson(selectSetPerson)

  const {
    data: person,
    isLoading: isLoadingPerson,
  } = useQuery<GetPersonResponse, AxiosError, GetPersonResponse>(
    ['getPerson', Number.parseInt(urlPersonId)],
    getPerson,
    {
      enabled: Boolean(Number.parseInt(urlPersonId)),
      onError: (error) => toast.error(
        error.response?.data?.error ||
        '⚠️ We`re unable to get this person at the moment. Please try again later.',
      ),
    })


  useEffect(() => {
    if (person) {
      setPerson(person)
    }
  }, [setPerson, person])


  const speciesId = useMemo(() => {
    if (!person?.species?.[0]) {
      return 0
    }

    const id = person.species[0].split('species/').pop()?.replace('/', '') || '0'
    return Number.parseInt(id)
  }, [person])


  const {
    data: species,
    isLoading: isLoadingSpecies,
  } = useQuery<GetSpeciesResponse, AxiosError, GetSpeciesResponse>(
    ['getSingleSpecies', speciesId],
    getSingleSpecies,
    {
      enabled: Boolean(speciesId),
      onError: (error) => toast.error(
        error.response?.data?.error ||
        '⚠️ We`re unable to get this person species at the moment. Please try again later.',
      ),
    })


  const isLoading = isLoadingPerson || isLoadingSpecies


  return (
    <div className="text-center">
      <div className="font-bold text-3xl">
        Star Wars notable character
      </div>

      <PersonTable
        className="mt-16"
        person={person}
        species={species}
        isLoading={isLoading}
      />

      <div className="border-t border-gray-300 m-auto my-16 w-192" />

      <Buttons
        className="mt-16"
        isLoading={isLoading}
      />
    </div>
  )
}

export default People
