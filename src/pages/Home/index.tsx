const Home = () => (
  <div>
    <div className="font-bold text-2xl">
      React template
    </div>
    <div className="my-6">
      This project is meant to be used a simple template to create scalable React Apps.
    </div>

    <div className="font-bold text-lg mb-2">
      Running locally
    </div>
    <code>
      git clone git@gitlab.com:IvanDimanov/react-template.git<br />
      cd react-template<br />
      yarn<br />
      yarn start
    </code>

    <div className="font-bold text-lg mt-6 mb-2">
      Tech stack
    </div>

    <ul className="space-y-2">
      <li>
        - React with
        {' '}
        <a
          className="text-gray-600 hover:text-black"
          href="https://create-react-app.dev"
          target="_blank"
          rel="noopener noreferrer"
        >
          CRA
        </a>
        {' '}
        +
        {' '}
        <a
          className="text-gray-600 hover:text-black"
          href="https://www.typescriptlang.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          TypeScript
        </a>
      </li>

      <li>
        -
        {' '}
        <a
          className="text-gray-600 hover:text-black"
          href="https://react-query.tanstack.com"
          target="_blank"
          rel="noopener noreferrer"
        >
          React-Query
        </a>
        {' '}
         +
        {' '}
        <a
          className="text-gray-600 hover:text-black"
          href="https://github.com/pmndrs/zustand"
          target="_blank"
          rel="noopener noreferrer"
        >
          Zustand
        </a>
      </li>
      <li>
        -
        {' '}
        <a
          className="text-gray-600 hover:text-black"
          href="https://tailwindcss.com"
          target="_blank"
          rel="noopener noreferrer"
        >
          TailwindCSS
        </a>
      </li>
    </ul>
  </div>
)

export default Home
